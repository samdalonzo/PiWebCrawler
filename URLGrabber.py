import urllib.request
from bs4 import BeautifulSoup
import csv
'''
- Summarize the massive amount of information on the MIT Media Lab site
    - Specifically, this scraper compiles links for every group of the lab
    - The links are then stored in a file and are to be used by other crawlers
- Serve as a base for an eventual Docker Based web crawler
'''

url = 'https://www.media.mit.edu/research/?filter=groups'
base_url = 'https://www.media.mit.edu'
page = urllib.request.urlopen(url)
soup = BeautifulSoup(page, 'html.parser')

my_list = soup.find_all('div', attrs= {'class':'module container-item variant-group'})

'''
#Write to CSV file
csvfile = open('urlstoscrape.csv', 'w')
fieldnames = ['Description', 'URL']
csvwriter = csv.writer(csvfile, dialect = 'excel', delimiter = '|')
csvwriter.writerow(('Description', 'URLS'))
'''

#Write to a text file
textfile = open('urlstoscrape.txt', 'w')

for item in my_list:
    title = item.find('div', attrs ={'class':'module-title'}).get_text()
    my_links = []
    for link in item.find_all('a'):
        textfile.write(base_url + link.get('href'))
        textfile.write('\n')

textfile.close()