import urllib.request
from multiprocessing import Pool, Queue
from bs4 import BeautifulSoup


class ResultManager():
    '''
    This node will manage the results passed to it by multiple workers
    It will receive results and links from the worker nodes
    It will send links that have been filtered to the server node
    It will write the results to one central database (eventually), for now it will write them to a CSV file

    Thoughts:
        This ResultManager will be listening for incoming messages from the worker nodes
        How do I utilize a database for storing results?
        Should the server be the one filtering the links? (Probably)
    '''

    resultFile = open('ScraperResults.csv', 'w')
    csvwriter = csv.writer(csvfile, dialect='excel', delimiter='|')
    newLinks = []

    def receiveResult(self):
        '''
        :return results:
        '''


    def writeResults(self, results):
        '''
        :param results:
        :return Nothing:
        Write the results to a csv file
        '''

    def filterLinks(self, links):
        '''
        :param links [array]:
        :return cleanedLinks:
        Filter out links to pages blocked by
        '''

    def sendLinks(self, links):
        '''
        :param links:
        :return Nothing:
        Send links to the server
        '''
