import urllib.request
from multiprocessing import Pool, Queue
from bs4 import BeautifulSoup



class Worker():
    '''
    This is the crawler or worker node.
    Will receive a URL from the server and scrape the page depending on [flexible] goal of project.
    This is a task with a specific end goal. Once the worker has scraped the page,
        it will send the results [or lack thereof] to the ResultManager
    Once the worker has sent the results,
        it will broadcast to the Server that it has finished its current task and is ready for a new one
    It will receive links from the Server node and send results/links to the results node

    Thoughts:
        In this very simple initial use case, the worker will only be scraping for the title and will return the title as a string
        Need to fine tune understanding of receiving and sending messages between nodes.

    '''

    def receiveUrl(self):
        '''
        :return:
        recieve Url from Server
        '''

    def getSoup(self, url):
        '''
        :param url:
        :return soup:
        Passed url as a str, returns HTML soup from the URL
        '''

    def scrapeSoup(self, soup):
        '''
        :param soup:
        :return str:
        '''

    def getLinks(self, soup):
        '''
        :param soup:
        :return array of links:
        Thoughts:
            Will need to mitigate the fact that scraped URLs may not have a 'base url'
        '''

    def sendLinks(self, links):
        '''
        :param links:
        :return:

        '''

    def sendResults(self, results):
        '''
        :param results:
        :return:
        '''

    def broadcastFinish(self):
        '''
        :return:
        When the node has sent the results to the resultManager,
            it will broadcast to the server that it is ready for a new task
        '''