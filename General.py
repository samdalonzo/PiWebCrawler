import urllib.request
from multiprocessing import Pool, Queue
from bs4 import BeautifulSoup

'''

These are general functions that will be used to scrape group pages on the MIT Media Lab Website
From the overview page, this scraper . . . 
- Lists 5 most recent publications
- Gets name and email of leader of group
- Gets the following information for 15 projects
    - Title
    - Research Topic Tags

'''

queueFile = open('urlstoscrape.txt', 'r')
crawledFile = open('crawledurls.txt', 'r+')

#Get Soup for Page
def getNewSoup(queueFile):
    tempCounter = 0

    #Check that the link has not been crawled
    for newLink in queueFile:
        for crawledLink in crawledFile:
            if newLink == crawledLink:
                tempCounter = 1
        if tempCounter == 1:
            pass
        else:
            url = newLink
            page = urllib.request.urlopen(url)
            soup = BeautifulSoup(page, 'html.parser')
            crawledFile.write(url)
            return soup
            break
        tempCounter = 0


#Scrape relevant information from Page. This function can vary from use case to use case
def scrapePage(soup):

    return soup.head()

#Check if url has been crawled
def checkUrl(url):
    return url

#Get links from soup, need to check for base_url
#Potentially write straight to the queue file
def getLinks(soup):
    new_links = []
    for link in soup.find_all('a'):
        new_links.append(link)
    return new_links

#We don't want to collect every single link from every page
#Example: wwww.google.com
#Check if URLs have bot blocking system in place
def filterLinks():


#Send results from worker (Crawler) to Result node
def reportResults():


#Test Functions
mySoup = getNewSoup(queueFile)
print(scrapePage(mySoup))
queueFile.close()
crawledFile.close()
